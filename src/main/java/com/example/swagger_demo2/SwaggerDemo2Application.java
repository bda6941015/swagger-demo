package com.example.swagger_demo2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SwaggerDemo2Application {

	public static void main(String[] args) {
		SpringApplication.run(SwaggerDemo2Application.class, args);
	}

}
